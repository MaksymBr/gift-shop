var express = require('express');
var router = express.Router();
var Cart = require('../models/cart');

var Product = require('../models/product');
var Order = require('../models/order');
var controllers = require('../controllers/index');
/* GET home page. */
router.get('/', controllers.mainPage);

router.get('/add-to-cart/:id', controllers.addToCart);

router.get('/reduce/:id', controllers.reduce);

router.get('/remove/:id', controllers.removeById);

router.get('/shopping-cart', controllers.shoppingCart);

router.get('/checkout', isLoggedIn, controllers.getCheckout);

router.get('/about-us', controllers.aboutUs);

router.get('/contacts', controllers.contacts);

router.get('/products/:id', controllers.productsById);

router.post('/checkout', isLoggedIn, controllers.checkOut);

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  req.session.oldUrl = req.url;
  res.redirect('/user/signin');
}
